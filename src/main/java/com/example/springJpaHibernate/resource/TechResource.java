package com.example.springJpaHibernate.resource;

import com.example.springJpaHibernate.model.Tech;
import com.example.springJpaHibernate.repository.TechRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/tech")
public class TechResource {

    @Autowired
    TechRepository techRepository;

    /**
     * Returns all tech
     * @return
     */
    @GetMapping("/getAllTech")
    public List<Tech> getAll() {
        return techRepository.findAll();
    }

    /**
     * Returns tech with given id
     * @param id
     * @return
     */
    @GetMapping("/getByTechId/{id}")
    public Tech getByTechId(
            @PathVariable final Integer id) {
        return techRepository.getOne(id);
    }

    /**
     * Returns highest value tech or multiple if there are tech with same max values
     * @return
     */
    @GetMapping("/getExpensiveTech")
    public List<Tech> getExpensiveTech() {
        return techRepository.getExpensive();
    }

    /**
     * Inserts new tech
     * @param name
     * @param value
     * @return
     */
    @GetMapping("/insertTech/{name}/{value}")
    public Tech insertNewTech(
            @PathVariable final String name,
            @PathVariable final Integer value) {

        Tech tech = new Tech();

        tech.setName(name);
        tech.setValue(value);

        return techRepository.save(tech);
    }

    /**
     * Edits tech value
     * @param id
     * @param value
     * @return
     */
    @GetMapping("/editTechValue/{id}/{value}")
    public Tech editTechValue(
            @PathVariable final Integer id,
            @PathVariable final Integer value) {

        Tech tech = techRepository.getOne(id);

        if(tech != null) {
            tech.setValue(value);
        }

        return techRepository.save(tech);
    }

    /**
     * Deletes tech
     * @param id
     */
    @GetMapping("/deleteTech/{id}")
    public void deleteTech(
            @PathVariable final Integer id) {

        Tech tech = techRepository.getOne(id);

        techRepository.delete(tech);
    }

}

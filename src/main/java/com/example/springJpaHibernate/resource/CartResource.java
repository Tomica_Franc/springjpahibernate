package com.example.springJpaHibernate.resource;

import com.example.springJpaHibernate.model.Cart;
import com.example.springJpaHibernate.model.Items;
import com.example.springJpaHibernate.repository.CartRepository;
import com.example.springJpaHibernate.repository.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/cart")
public class CartResource {

    @Autowired
    CartRepository cartRepository;

    @Autowired
    ItemsRepository itemsRepository;

    @GetMapping("/getAll")
    public List<Cart> getAllCarts() {
        return cartRepository.findAll();
    }

    @GetMapping("/getCartItems/{id}")
    public Set<Items> getItemsForCart(
            @PathVariable final Integer id) {
        return cartRepository.getOne(id).getItems();
    }

    @GetMapping("/deleteCart/{id}")
    public void deleteCart(
            @PathVariable final Integer id) {

        Cart cart = cartRepository.getOne(id);

        cartRepository.delete(cart);
    }

    @GetMapping("/createEmptyCart/{name}")
    public Cart createCart(
            @PathVariable final String name) {

        Cart cart = new Cart();
        cart.setName(name);

        return cartRepository.save(cart);
    }

    @GetMapping("/editCartTotal/{id}/{total}")
    public Cart editCartTotal(
            @PathVariable final Integer id,
            @PathVariable final double total) {

        Cart cart = cartRepository.getOne(id);

        cart.setTotal(total);

        return cartRepository.save(cart);
    }

    @GetMapping("/setItemToCart/{id}/{itemName}/{itemTotal}/{quantity}")
    public Cart setItemToCart(
            @PathVariable final Integer id,
            @PathVariable final String itemName,
            @PathVariable final double itemTotal,
            @PathVariable final Integer quantity) {

        Cart cart = cartRepository.getOne(id);

        Set<Items> items = cart.getItems();

        Items item = new Items();
        item.setCart(cart);
        item.setItemId(itemName);
        item.setItemTotal(itemTotal);
        item.setQuantity(quantity);

        itemsRepository.save(item);

        items.add(item);

        cart.setItems(items);

        return cartRepository.save(cart);
    }

    @GetMapping("/removeItemFromCart/{id}/{itemId}")
    public Cart removeItem(
            @PathVariable final Integer id,
            @PathVariable final Integer itemId) {

        Cart cart = cartRepository.getOne(id);

        Set<Items> items = cart.getItems();

        for (Items item: items  ) {
            if(item.getId() == itemId) items.remove(item);
        }

        cart.setItems(items);

        return cartRepository.save(cart);
    }
}

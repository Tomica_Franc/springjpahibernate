package com.example.springJpaHibernate.resource;

import com.example.springJpaHibernate.model.Items;
import com.example.springJpaHibernate.repository.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/items")
public class ItemsResource {

    @Autowired
    ItemsRepository itemsRepository;

    @GetMapping("/getAll")
    public List<Items> getAllItems() {
        return itemsRepository.findAll();
    }

    @GetMapping("/getItem/{id}")
    public Items getItem(
            @PathVariable final Integer id) {
        return itemsRepository.getOne(id);
    }

    @GetMapping("/changeItemName/{id}/{name}")
    public Items changeItemName(
            @PathVariable final Integer id,
            @PathVariable final String name) {

        Items items = itemsRepository.getOne(id);

        items.setItemId(name);

        return itemsRepository.save(items);
    }

    @GetMapping("/changeItemTotal/{id}/{total}")
    public Items changeTotal(
            @PathVariable final Integer id,
            @PathVariable final double total) {

        Items items = itemsRepository.getOne(id);

        items.setItemTotal(total);

        return itemsRepository.save(items);
    }
}

package com.example.springJpaHibernate.resource;

import com.example.springJpaHibernate.model.User;
import com.example.springJpaHibernate.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UsersResource {

    @Autowired
    UsersRepository usersRepository;

    /**
     * Returns all users
     * @return
     */
    @GetMapping("/getAll")
    public List<User> getAll() {
        return usersRepository.findAll();
    }

    /**
     * Returns all users with same name
     * @param name
     * @return
     */
    @GetMapping("/getAllByName/{name}")
    public List<User> getUsersByName(
            @PathVariable final String name) {

        Optional<List<User>> optionalList = usersRepository.findByName(name);

        List<User> users = optionalList
                .orElse(new ArrayList<>());

        return users;
    }

    /**
     * Returns user by id or throws exception if user doesn't exist
     * @param id
     * @return
     */
    @GetMapping("/getUserById/{id}")
    public User getUserById(
            @PathVariable final Integer id) {
        Optional<User> optional = usersRepository.findById(id);

        return optional
                .orElseThrow(() -> new RuntimeException("No user found by given id"));
    }

    /**
     * Updates salary for user
     * @param id
     * @param salary
     * @return
     */
    @GetMapping("/updateSalayForUser/{id}/{salary}")
    public User updateUserSalary(
            @PathVariable final Integer id,
            @PathVariable final Integer salary) {

        User user = usersRepository.getOne(id);

        if(user != null) {
            user.setSalary(salary);
        }

        return usersRepository.save(user);
    }

    /**
     * Deletes user by given id
     * @param id
     */
    @GetMapping("/deleteUser/{id}")
    public void deleteUser(
            @PathVariable final Integer id) {

        User user = usersRepository.getOne(id); //if user doesn't exist then it will return 500 internal server error

        if(user != null) {
            usersRepository.delete(user);
        }
    }

    /**
     * Inserts new user in database
     * @param name
     * @param salary
     * @param gender
     * @return
     */
    @GetMapping("/insertNewUser/{name}/{salary}/{gender}")
    public User insertUser(
            @PathVariable final String name,
            @PathVariable final Integer salary,
            @PathVariable final char gender) {

        User user = new User();

        user.setName(name);
        user.setSalary(salary);
        user.setGender(gender);

        return usersRepository.save(user);
    }
}

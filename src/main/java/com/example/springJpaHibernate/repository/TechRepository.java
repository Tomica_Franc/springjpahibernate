package com.example.springJpaHibernate.repository;

import com.example.springJpaHibernate.model.Tech;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface TechRepository extends JpaRepository<Tech, Integer> {

    @Query("select t from Tech t where t.value = (select max(te.value) from Tech te)")
    List<Tech> getExpensive();
}

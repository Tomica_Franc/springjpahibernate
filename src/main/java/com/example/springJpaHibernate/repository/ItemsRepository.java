package com.example.springJpaHibernate.repository;

import com.example.springJpaHibernate.model.Items;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemsRepository extends JpaRepository<Items, Integer> {

}

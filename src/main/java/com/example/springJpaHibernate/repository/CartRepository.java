package com.example.springJpaHibernate.repository;

import com.example.springJpaHibernate.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Integer> {

}

package com.example.springJpaHibernate.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "items", catalog = "usersdata")
public class Items {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
//    private Integer cartId;
    private String itemId;
    private double itemTotal;
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name="cart_id", nullable=false)
    @JsonBackReference
    private Cart cart;

    public Items() {
    }

    public Items(String itemId, double itemTotal, Integer quantity, Cart cart) {
        this.itemId = itemId;
        this.itemTotal = itemTotal;
        this.quantity = quantity;
        this.cart = cart;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//    public Integer getCartId() {
//        return cartId;
//    }
//
//    public void setCartId(Integer cartId) {
//        this.cartId = cartId;
//    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public double getItemTotal() {
        return itemTotal;
    }

    public void setItemTotal(double itemTotal) {
        this.itemTotal = itemTotal;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
}
